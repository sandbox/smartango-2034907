<?php

/**
 * @file
 * Implement an runyourjs field, based on the file module's file field.
 */

/**
 * Implements hook_field_info().
 */
function runyourjs_field_info() {
  return array(
    'runyourjs' => array(
      'label' => t('runyourjs'),
      'description' => t('This field stores the ID of an runyourjs file as an integer value.'),
      'settings' => array(
        'uri_scheme' => variable_get('file_default_scheme', 'public'),
        'default_runyourjs' => 0,
      ),
      'instance_settings' => array(
        'file_extensions' => 'js json',
        'file_directory' => '',
        'max_filesize' => '',
        'alt_field' => 0,
        'title_field' => 0,
        'default_runyourjs' => 0,
      ),
      'default_widget' => 'runyourjs_runyourjs',
      'default_formatter' => 'runyourjs',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function runyourjs_field_settings_form($field, $instance) {
  $defaults = field_info_field_settings($field['type']);
  $settings = array_merge($defaults, $field['settings']);

  $scheme_options = array();
  foreach (file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE) as $scheme => $stream_wrapper) {
    $scheme_options[$scheme] = $stream_wrapper['name'];
  }
  $form['uri_scheme'] = array(
    '#type' => 'radios',
    '#title' => t('Upload destination'),
    '#options' => $scheme_options,
    '#default_value' => $settings['uri_scheme'],
    '#description' => t('Select where the final files should be stored. Private file storage has significantly more overhead than public files, but allows restricted access to files within this field.'),
  );

  // When the user sets the scheme on the UI, even for the first time, it's
  // updating a field because fields are created on the "Manage fields"
  // page. So runyourjs_field_update_field() can handle this change.
  $form['default_runyourjs'] = array(
    '#title' => t('Default runyourjs'),
    '#type' => 'managed_file',
    '#description' => t('If no runyourjs is uploaded, this runyourjs will be shown on display.'),
    '#default_value' => $field['settings']['default_runyourjs'],
    '#upload_location' => $settings['uri_scheme'] . '://ryjscripts/',
  );

  return $form;
}

/**
 * Implements hook_field_instance_settings_form().
 */
function runyourjs_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];

  // Use the file field instance settings form as a basis.
  $form = file_field_instance_settings_form($field, $instance);

  // Remove the description option.
  unset($form['description_field']);

  // Add title and alt configuration options.
  $form['alt_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable <em>Alt</em> field'),
    '#default_value' => $settings['alt_field'],
    '#description' => t('The alt attribute may be used by search engines, screen readers, and when the runyourjs cannot be loaded.'),
    '#weight' => 10,
  );
  $form['title_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable <em>Title</em> field'),
    '#default_value' => $settings['title_field'],
    '#description' => t('The title attribute is used as a tooltip when the mouse hovers over the runyourjs.'),
    '#weight' => 11,
  );

  // Add the default runyourjs to the instance.
  $form['default_runyourjs'] = array(
    '#title' => t('Default runyourjs'),
    '#type' => 'managed_file',
    '#description' => t("If no runyourjs is uploaded, this runyourjs will be shown on display and will override the field's default runyourjs."),
    '#default_value' => $settings['default_runyourjs'],
    '#upload_location' => $field['settings']['uri_scheme'] . '://ryjscripts/',
  );

  return $form;
}


/**
 * Implements hook_field_load().
 */
function runyourjs_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  file_field_load($entity_type, $entities, $field, $instances, $langcode, $items, $age);
}

/**
 * Implements hook_field_prepare_view().
 */
function runyourjs_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
  // If there are no files specified at all, use the default.
  foreach ($entities as $id => $entity) {
    if (empty($items[$id])) {
      $fid = 0;
      // Use the default for the instance if one is available.
      if (!empty($instances[$id]['settings']['default_runyourjs'])) {
        $fid = $instances[$id]['settings']['default_runyourjs'];
      }
      // Otherwise, use the default for the field.
      elseif (!empty($field['settings']['default_runyourjs'])) {
        $fid = $field['settings']['default_runyourjs'];
      }

      // Add the default runyourjs if one is found.
      if ($fid && ($file = file_load($fid))) {
        $items[$id][0] = (array) $file + array(
          'is_default' => TRUE,
          'alt' => '',
          'title' => '',
        );
      }
    }
  }
}

/**
 * Implements hook_field_presave().
 */
function runyourjs_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_presave($entity_type, $entity, $field, $instance, $langcode, $items);

  return;
  // Determine the dimensions if necessary.
  foreach ($items as &$item) {
    if (!isset($item['width']) || !isset($item['height'])) {
      $info = runyourjs_get_info(file_load($item['fid'])->uri);

      if (is_array($info)) {
        $item['width'] = $info['width'];
        $item['height'] = $info['height'];
      }
    }
  }
}

/**
 * Implements hook_field_insert().
 */
function runyourjs_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_insert($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_update().
 */
function runyourjs_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_update($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_delete().
 */
function runyourjs_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_delete($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_delete_revision().
 */
function runyourjs_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_is_empty().
 */
function runyourjs_field_is_empty($item, $field) {
  return file_field_is_empty($item, $field);
}

/**
 * Implements hook_field_widget_info().
 */
function runyourjs_field_widget_info() {
  return array(
    'runyourjs_runyourjs' => array(
      'label' => t('runyourjs'),
      'field types' => array('runyourjs'),
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function runyourjs_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  // Use the file widget settings form.
  $form = file_field_widget_settings_form($field, $instance);

  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function runyourjs_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  // Add display_field setting to field because file_field_widget_form() assumes it is set.
  $field['settings']['display_field'] = 0;

  $elements = file_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  $settings = $instance['settings'];

  foreach (element_children($elements) as $delta) {

    // If not using custom extension validation, ensure this is an runyourjs.
    $supported_extensions = array('js', 'json',);
    $extensions = isset($elements[$delta]['#upload_validators']['file_validate_extensions'][0]) ? $elements[$delta]['#upload_validators']['file_validate_extensions'][0] : implode(' ', $supported_extensions);
    $extensions = array_intersect(explode(' ', $extensions), $supported_extensions);
    $elements[$delta]['#upload_validators']['file_validate_extensions'][0] = implode(' ', $extensions);

    // Add all extra functionality provided by the runyourjs widget.
    $elements[$delta]['#process'][] = 'runyourjs_field_widget_process';
  }

  if ($field['cardinality'] == 1) {
    // If there's only one field, return it as delta 0.
    if (empty($elements[0]['#default_value']['fid'])) {
      $elements[0]['#description'] = theme('file_upload_help', array('description' => $instance['description'], 'upload_validators' => $elements[0]['#upload_validators']));
    }
  }
  else {
    $elements['#file_upload_description'] = theme('file_upload_help', array('upload_validators' => $elements[0]['#upload_validators']));
  }
  return $elements;
}

/**
 * An element #process callback for the runyourjs_runyourjs field type.
 *
 * Expands the runyourjs_runyourjs type to include the alt and title fields.
 */
function runyourjs_field_widget_process($element, &$form_state, $form) {
  $item = $element['#value'];
  $item['fid'] = $element['fid']['#value'];

  $instance = field_widget_instance($element, $form_state);

  $settings = $instance['settings'];
  $widget_settings = $instance['widget']['settings'];

  $element['#theme'] = 'runyourjs_widget';
  $element['#attached']['css'][] = drupal_get_path('module', 'runyourjs') . '/runyourjs.css';

  // Add the runyourjs preview.
  if ($element['#file']) {
    $variables = array(
      'path' => $element['#file']->uri,
    );

    // Determine runyourjs dimensions.
    if (isset($element['#value']['width']) && isset($element['#value']['height'])) {
      $variables['width'] = $element['#value']['width'];
      $variables['height'] = $element['#value']['height'];
    }
    else {
      //$info = runyourjs_get_info($element['#file']->uri);
      $info = array();

      if (is_array($info)) {
        //$variables['width'] = $info['width'];
        //$variables['height'] = $info['height'];
        $variables['width'] = $variables['height'] = 10;
      }
      else {
        $variables['width'] = $variables['height'] = NULL;
      }
    }

    $element['preview'] = array(
      '#type' => 'markup',
      '#markup' => theme('runyourjs_formatter', $variables),
    );

    // Store the dimensions in the form so the file doesn't have to be accessed
    // again. This is important for remote files.
    $element['width'] = array(
      '#type' => 'hidden',
      '#value' => $variables['width'],
    );
    $element['height'] = array(
      '#type' => 'hidden',
      '#value' => $variables['height'],
    );
  }


  return $element;
}

/**
 * Returns HTML for an runyourjs field widget.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element representing the runyourjs field widget.
 *
 * @ingroup themeable
 */
function theme_runyourjs_widget($variables) {
  $element = $variables['element'];
  $output = '';
  $output .= '<div class="runyourjs-widget form-managed-file clearfix">';

  if (isset($element['preview'])) {
    $output .= '<div class="runyourjs-preview">';
    $output .= drupal_render($element['preview']);
    $output .= '</div>';
  }

  $output .= '<div class="runyourjs-widget-data">';
  if ($element['fid']['#value'] != 0) {
    $element['filename']['#markup'] .= ' <span class="file-size">(' . format_size($element['#file']->filesize) . ')</span> ';
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Implements hook_field_formatter_info().
 */
function runyourjs_field_formatter_info() {
  $formatters = array(
    'runyourjs' => array(
      'label' => t('runyourjs'),
      'field types' => array('runyourjs'),
      'settings' => array('runyourjs_style' => '',),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function runyourjs_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  drupal_set_message('formatter_setting form is called');
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $runyourjs_styles = array('widget'=>'widget','autoload'=>'autoload');
  $element['runyourjs_style'] = array(
    '#title' => t('runyourjs style'),
    '#type' => 'select',
    //'#default_value' => $settings['runyourjs_style'],
      '#default_value' => 'widget',
    '#empty_option' => t('None (original runyourjs)'),
      //'#theme'=> 'runyourjs_formatter',
    '#options' => $runyourjs_styles,
  );

  $link_types = array(
      'file' => t('File'),
  );
  $element['image_link'] = array(
      '#title' => t('Link file to'),
      '#type' => 'select',
      '#default_value' => $settings['image_link'],
      '#empty_option' => t('Nothing'),
      '#options' => $link_types,
  );


  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function runyourjs_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();
  $summary[] = 'settings';

  // Display this setting only if runyourjs is linked.
  if (isset($settings['runyourjs_style'])) {
    $summary[] = $settings['runyourjs_style'];
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function runyourjs_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  foreach ($items as $delta => $item) {
      $uri = array(
        'path' => file_create_url($item['uri']),
        'options' => array(),
      );
    $element[$delta] = array(
      '#theme' => 'runyourjs_formatter',
      '#item' => $item,
      '#runyourjs_style' => $display['settings']['runyourjs_style'],
      '#path' => isset($uri) ? $uri : '',
    );
    $element['#attached']['js'][] = drupal_get_path('module', 'runyourjs') . '/runyourjs.js';
  }

  return $element;
}

/**
 * Returns HTML for an runyourjs field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: Associative array of runyourjs data, which may include "uri", "alt",
 *     "width", "height", "title" and "attributes".
 *   - runyourjs_style: An optional runyourjs style.
 *   - path: An array containing the link 'path' and link 'options'.
 *
 * @ingroup themeable
 */
function theme_runyourjs_formatter($variables) {
  //return print_r($variables,TRUE);
  $item = $variables['item'];
  $runyourjs = array(
    'path' => $item['uri'],
  );

  $path = $variables['path']['path'];

  //return l($path, $path);

  if (array_key_exists('alt', $item)) {
    $runyourjs['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $runyourjs['attributes'] = $item['attributes'];
  }

  // Do not output an empty 'title' attribute.
  if (isset($item['title']) && drupal_strlen($item['title']) > 0) {
    $runyourjs['title'] = $item['title'];
  }

  // The link path and link options are both optional, but for the options to be
  // processed, the link path must at least be an empty string.
  if (isset($variables['path']['path'])) {
    $path = $variables['path']['path'];
    $output = $variables['item']['filename'];
    $options = isset($variables['path']['options']) ? $variables['path']['options'] : array();
    // When displaying an runyourjs inside a link, the html option must be TRUE.
    $options['html'] = TRUE;
    $output = l($output, $path, $options);
    $script_url = file_create_url($variables['item']['uri']);
    if($variables['runyourjs_style'] == 'widget') {
      $load = " , " . '<a href="#" class="runyourjs-load" rel="'.$script_url.'">load</a>';
      $output .= $load;
    } else {
      drupal_add_js($script_url);
    }
  }

  return $output;
}
